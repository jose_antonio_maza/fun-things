/* 
    Get the last three (or n) items of the nested array:
*/


const nested = {
  value: 'Apple',
  next: {
    value: 'Pear',
    next: {
      value: 'Orange',
      next: {
        value: 'Penguin',
        next: {
          value: 'Red bull',
          next: {
            value: 'Something',
            next: null
          }
        }
      }
    }
  }
}

// using closures:
function solve(obj, lastItems = 3) {
  let remem = []

  function realSolver(obj) {
    if (obj.next === null) {
      remem.push(obj.value)
      return remem.slice(-lastItems)
    }
    remem.push(obj.value)
    return realSolver(obj.next)
  }
  return realSolver(obj)
}

console.log(solve(nested))