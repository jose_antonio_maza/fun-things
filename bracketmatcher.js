function BracketMatcher(str) {
  if (str.length === 0) return 1;
  // return 1 if correct, 0 if bad.

  //return totalCount(str, 0) === 0 ? 1 : 0
  return totalCount(str) === 0 ? 1 : 0
}


// original way I solved the 'leetcode' challenge:

/*function totalCount(str, curTotal) {
  if (str.length === 0) return 0
  if (curTotal < 0) return 0
  if (str[0] === '(') return 1 + totalCount(str.substring(1), curTotal + 1);
  if (str[0] === ')') return totalCount(str.substring(1), curTotal - 1) - 1;
  return 0 + totalCount(str.substring(1), curTotal);
}*/


// I tried again, but using closures:   (Not the most readable code, but I thought it would be cool to try)

function totalCount(str, initial = 0) {
  let curTotal = initial;
  function tot(str) {
    if (str.length === 0) return 0
    if (curTotal < 0) return 0
    if (str[0] === '(') {
      ++curTotal;
      return 1 + tot(str.substring(1));
    }
    if (str[0] === ')') {
      --curTotal;
      return tot(str.substring(1)) - 1;
    }
    return 0 + tot(str.substring(1));
  }
  return tot(str);

}

// keep this function call here 
console.log(BracketMatcher("he(llo)ddd)")); // should be 0
console.log(BracketMatcher("h(e(llo)ddd)")); // should be 1
console.log(BracketMatcher("(h(e(llo)ddd)")); // should be 0