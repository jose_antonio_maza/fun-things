/**
 * Exercise: Find the Routes a secret agent uses to travel. For example, given an "itinerary" of routes, 
 * find the all the routes taken by the agent to reach its final destination.
*/


// Original code: 

function findRoutesInitialCode(routes) {
  return routes
}

// My solution below:

/*
  Connects Travel Routes from Agent, converting them into a in-order list of places to travel
  @param routes: Array<Array<String>> - Array of itineraries
  @return String - An ordered, comma-separated list of destinations 
*/
function findRoutes(routes) {
  let starts = [];
  let exits = [];
  let mappedRoutes = new Map();

  for (i in routes) {
    starts.push(routes[i][0]);
    exits.push(routes[i][1]);
    mappedRoutes.set(routes[i][0], routes[i][1]);
  }

  const startingPoint = getStartingPoint(starts, exits);

  return findPath(mappedRoutes, startingPoint)
}

/*
  Finds starting point between a list of starts and a list of destinations.
  (It looks for which string does not appear in the list of destinations or exits)
  @param starts: Array<String> - Array of starting routes
  @param exits: Array<String> - Array of destination routes or "exits"
  @return String - Starting Point of Route
*/
function getStartingPoint(starts, exits) {
  let startingPoint = null

  for (i of starts) {
    if (exits.indexOf(i) === -1) {
      startingPoint = i
    }
  }

  return startingPoint
}

/*
  Finds path taken by spy
  @param map: Map - Map object that holds key-value pairs of destinations as Strings
  @param startingPoint: String - Starting destination of Route
  @return String - An ordered, comma-separated list of destinations
*/
function findPath(map, startingPoint) {
  if (map.has(startingPoint)) {
    let next = map.get(startingPoint);
    return startingPoint + ", " + findPath(map, next);
  }
  else {
    return startingPoint;
  }
}



// Tests:

var arr2 = [["Chicago", "Winnipeg"], ["Halifax", "Montreal"], ["Montreal", "Toronto"], ["Toronto", "Chicago"], ["Winnipeg", "Seattle"]]
var arr1 = [["MNL", "TAG"], ["CEB", "TAC"], ["TAG", "CEB"], ["TAC", "BOR"]]

console.log(findRoutes(arr2))