/*
 You are given a string with with a bunch of numbers and you need to order them by "weight"
A weight is the sum of its digits. for example, 11 = 2, 2000 = 2, 123 = 6, 9999 = 36. 
However, if two weights are the same, order the numbers by their 'string' value. 
*/

//Initial code: 

function orderWeightInitialCode(strng) {
 
  return strng
}


// My solution:

function orderWeight(strng) {
  if (strng === "") {
    return ""
  }
  let listOfIntegers = strng.split(' ')

  const weightedIntegersObj = listOfIntegers.map(integer => {
    return { "number": integer, "weight": getWeight(integer) }
  })

  const orderedIntegers = weightedIntegersObj.sort(compare).map(w => w.number);

  return orderedIntegers.join(" ")
}

function compare(a, b) {
  let base = a.weight - b.weight
  if (base === 0) {
    base = a.number.localeCompare(b.number)
  }
  return base
}

function getWeight(numberStr) {
  const digits = numberStr.split('').map(digit => +digit)
  const weight = digits.reduce((accWeight, digit) => accWeight + digit)
  return weight
}


/*
 Tests:
*/

console.log(orderWeight("103 123 4444 99 2000"))
console.log(orderWeight("56 65 74 100 99 68 86 180 90"))
console.log(orderWeight("2000 10003 1234000 44444444 9999 11 11 22 123"))